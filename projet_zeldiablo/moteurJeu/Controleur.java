package moteurJeu;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * classe qui represente un controleur en lien avec un KeyListener
 * 
 * @author vthomas
 * 
 */
public class Controleur implements KeyListener {

	/**
	 * commande en cours
	 */
	private Commande commandeEnCours;
	/**
	 * commande a retourner la difference avec la commandeencours vient du fait
	 * qu'on veut memoriser une touche appuyee
	 */
	private  Commande commandeARetourner;

	/**
	 * construction du controleur par defaut le controleur n'a pas de commande
	 */
	public Controleur() {
		this.commandeEnCours = new Commande();
		this.commandeARetourner = new Commande();
	}

	/**
	 * quand on demande les commandes, le controleur retourne la commande en
	 * cours
	 * 
	 * @return commande faite par le joueur
	 */
	public Commande getCommande() {
		Commande aRetourner = this.commandeARetourner;
		this.commandeARetourner = new Commande(this.commandeEnCours);
		return (aRetourner);
	}

	@Override
	/**
	 * met a jour les commandes en fonctions des touches appuyees
	 */
	public void keyPressed(KeyEvent e) {

		// si on appuie sur 'q',commande joueur est gauche
		// si on appuie sur 'd',commande joueur est droite
		// si on appuie sur 'z',commande joueur est haut
		// si on appuie sur 's',commande joueur est bas
		switch (e.getKeyChar()) {
			case 'q' -> {
				this.commandeEnCours.gauche = true;
				this.commandeARetourner.gauche = true;
			}
			case 'd' -> {
				this.commandeEnCours.droite = true;
				this.commandeARetourner.droite = true;
			}
			case 'z' -> {
				this.commandeEnCours.haut = true;
				this.commandeARetourner.haut = true;
			}
			case 's' -> {
				this.commandeEnCours.bas = true;
				this.commandeARetourner.bas = true;
			}
			case 'f' -> {
				this.commandeEnCours.attaquer = true;
				this.commandeARetourner.attaquer = true;
			}
			case 'e' ->{
				this.commandeEnCours.prendre = true;
				this.commandeARetourner.prendre = true;
			}
			case 'a'->{
				this.commandeEnCours.descendre = true;
				this.commandeARetourner.descendre = true;
			}
		}
	}

	@Override
	/**
	 * met a jour les commandes quand le joueur relache une touche
	 */
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyChar()) {
			case 'q' -> this.commandeEnCours.gauche = false;
			case 'd' -> this.commandeEnCours.droite = false;
			case 'z' -> this.commandeEnCours.haut = false;
			case 's' -> this.commandeEnCours.bas = false;
			case 'f' -> this.commandeEnCours.attaquer = false;
			case 'e' -> this.commandeEnCours.prendre = false;
			case 'a' -> this.commandeEnCours.descendre = false;
		}
	}

	@Override
	/**
	 * ne fait rien
	 */
	public void keyTyped(KeyEvent e) {

	}

}
