package Classe;


import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Labyrinthe{

    /**
     * créer un labyrinthe avec un tableau a double entrées
     */
    private Cases[][] laby;
    /**
     * taille horizontale du labyrinthe
     */
    private int tailleX;
    /**
     * taille verticale du labyrinthe
     */
    private int tailleY;

    /**
     * Liste d'entite Vivante appartenant
     */
    private ArrayList<Monstre> listEntite = new ArrayList<Monstre>();
    /**
     * Aventurier dans le labyrinthe
     */
    private Aventurier av;
    /**
     * Amulette permettant de sortir du labyrinthe
     */
    private Amulette am;
    /**
     * constructeur d'un labyrinthe
     * @param x taille horizontale
     * @param y taille verticale
     */
    public Labyrinthe(int x, int y){
        this.tailleX = x;
        this.tailleY = y;
        this.laby = new Cases[tailleX][tailleY];
        for(int i=0; i<tailleX ; i++){
            for(int j=0; j<tailleY;j++){
                this.laby[i][j]=new CaseLibre(i,j);
            }
        }
        for(int i = 0; i<tailleX;i++){
            laby[0][i] = new CasePleine(0,i);
            laby[i][0] = new CasePleine(i,0);
            laby[tailleX-1][i] = new CasePleine(tailleX-1,i);
            laby[i][tailleY-1] = new CasePleine(i,tailleY-1);
        }
        this.am = new Amulette(1,1);
        laby[2][2].setPiege(true);
    }
    /**
     * Constructeur d'un labyrinthe a partir d'un fichier texte
     * @Param nomfich correspond au nom du niveau
     * @Param nom correspond au nom de l'aventurier
     */
    public Labyrinthe(String nomfich,Aventurier av){
        try {
            BufferedReader lecteur = new BufferedReader(new FileReader("projet_zeldiablo/Niveaux/"+nomfich));
            String ligne;
            this.tailleX = Integer.parseInt(lecteur.readLine());
            this.tailleY = Integer.parseInt(lecteur.readLine());
            this.laby = new Cases[tailleX][tailleY];
            int nbligne= 0;          //y
            this.av =  av;
            while((ligne = lecteur.readLine()) != null){

                int nbcharactere = 0;    //x
                for (int i=0;i<ligne.length(); i++){
                    char ch= ligne.charAt(i);
                    char mur= 'X';

                    if (ch==mur) {
                        this.laby[nbcharactere][nbligne] = new CasePleine(nbcharactere,nbligne);
                    }else{
                        this.laby[nbcharactere][nbligne] = new CaseLibre(nbcharactere,nbligne);
                    }
                    if(ch=='M'){
                        listEntite.add(new MonstreImmobile(nbcharactere,nbligne,this));
                    }
                    if(ch=='P'){
                        this.laby[nbcharactere][nbligne].setPiege(true);
                    }
                    if(ch=='A'){
                        this.av.setCoordonneX(nbcharactere);
                        this.av.setCoordonneY(nbligne);
                        this.getCase(nbcharactere, nbligne).setDebut(true);
                    }
                    if(ch=='R'){
                        listEntite.add(new MonstreAleatoire(nbcharactere,nbligne,this));
                    }
                    if(ch=='I'){
                        listEntite.add(new MonstreAttire(nbcharactere,nbligne,this));
                    }
                    if(ch=='U'){
                        this.am = new Amulette(nbcharactere,nbligne);
                    }
                    if(ch=='E'){
                        this.getCase(nbcharactere, nbligne).setEscalier(true);
                    }
                    nbcharactere ++;
                }
                nbligne ++;

            }
            lecteur.close();
        }catch( IOException io){
            this.tailleX = 10;
            this.tailleY = 10;
            this.laby = new Cases[tailleX][tailleY];
            for(int i=0; i<tailleX ; i++){
                for(int j=0; j<tailleY;j++){
                    this.laby[i][j]=new CaseLibre(i,j);
                }
            }
            for(int i = 0; i<tailleX;i++){
                laby[0][i] = new CasePleine(0,i);
                laby[i][0] = new CasePleine(i,0);
                laby[tailleX-1][i] = new CasePleine(tailleX-1,i);
                laby[i][tailleY-1] = new CasePleine(i,tailleY-1);
            }
            this.av = av;
            this.getCase(av.getCoordonneX(), av.getCoordoneeY()).setDebut(true);
            laby[2][2].setPiege(true);
            this.am = new Amulette(1,8);
        }
    }

    /**
     * supprime un monstre mort de la liste
     * de monstre du labyrinthe
     */
    public void retirerMort(){
        for (int i = 0; i < listEntite.size(); i++) {
            if (!listEntite.get(i).isVivant()){
                listEntite.remove(i);
            }
        }
    }

    /**
     * permet de récupérer la case a des coordonnées données
     * @param x position x de la case voulue
     * @param y position y de la case voulue
     * @return la case à la position demandée
     */
    public Cases getCase(int x, int y){
        Cases c = new CasePleine();
        if(x>=0 && y>= 0 && x <= tailleX && y<= tailleY){
            Cases c2 =  this.laby[x][y];
            return c2;
        }
        return c;
    }

    /**
     * permet de récupérer l'attribut privé tailleX
     * @return l'attribut tailleX
     */
    public int getTailleX() {
        return tailleX;
    }

    /**
     * permet de récupérer l'attribut privé tailleY
     * @return l'attribut tailleY
     */
    public int getTailleY() {
        return tailleY;
    }

    /**
     * permet d'ajouter un monstre dans la
     * liste de monstres du labyrinthe
     * @param e le monstre à ajouter
     */
    public void addList(Monstre e){
        listEntite.add(e);
    }

    /**
     * permet de retourner le monstre à une position
     * i de la liste de monstres
     * @param i la position dans la liste
     * @return le monstre à la position donnée
     */
    public Monstre getEntite(int i){
        return listEntite.get(i);
    }

    /**
     * retourne un monstre à des coordonnées données
     * @param x position x
     * @param y position y
     * @return le monstre à la position (null si pas de monstre)
     */
    public Monstre getMonstre(int x, int y){
        Monstre e = null;
        for (Monstre eV : listEntite) {
            if (eV.getCoordonneX() == x && eV.getCoordoneeY() == y) {
                e = eV;
            }
        }
        return e;
    }

    /**
     * permet de connaitre le nombre de monstres présents dans le labyrinthe
     * @return le nombre de monstres
     */
    public int getTailleListe(){
        return listEntite.size();
    }

    /**
     * permet de récupérer la liste des montres présents dans le labyrinthe
     * @return la liste de monstres
     */
    public ArrayList<Monstre> getListEntite() {
        return listEntite;
    }

    /**
     * permet de récupérer l'attribut privé av
     * @return l'aventurier dans le labyrinthe
     */
    public Aventurier getAv() {
        return av;
    }
    /**
     * permet de récupérer l'amulette
     * @return Amulette
     */
    public Amulette getAm() {
        return am;
    }
}
