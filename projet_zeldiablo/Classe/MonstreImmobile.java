package Classe;

import moteurJeu.Commande;
import javax.swing.*;

public class MonstreImmobile implements Monstre{

    /**
     * points de vie du monstre
     */
    private int pv;
    /**
     * coordonnées du monstre dans le labyrinthe
     */
    private int coordonneX,coordonneY;
    /**
     * puissance d'attaque du monstre
     */
    private int degat;
    /**
     * portée d'attaque du monstre
     */
    private final int portee =  1;
    /**
     * labyrinthe où se trouve le monstre
     */
    private Labyrinthe lab;
    /**
     * indique si le monstre est en vie ou non
     */
    private boolean vivant;
    /**
     * int permettant de récupérer le nombre de boucles réalisées
     */
    private int nbBoucle = 0;

    /**
     * boolean pour savoir si le monstre a subit des dégats
     */
    private boolean blesser;


    /**
     * constructeur d'un monstre
     * @param x position x
     * @param y position y
     * @param laby labyrinthe où se trouve le monstre
     */
    public MonstreImmobile(int x, int y, Labyrinthe laby){
        this.coordonneX = x;
        this.coordonneY = y;
        this.degat =1;
        this.pv = 10;
        this.vivant = true;
        this.lab = laby;
        this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
    }


    @Override
    public void seDeplacer() {
        //vide car le monstre ne se déplace pas
    }

    @Override
    public void seDeplacerVersAcenturier(Commande c) {
        //vide car le monstre ne se déplace pas
    }

    /**
     * permet au monstre d'attaquer
     * @param aventurier l'aventurier qui subira l'attaque
     * @return true si l'attaque a été portée
     */
    public boolean attaquer(Aventurier aventurier){
        if(this.vivant) {
            boolean res = false;
                if (aventurier.getLab() != this.lab) {
                    res = false;
                } else if(this.nbBoucle == 10) {
                    int x = aventurier.getCoordonneX();
                    int y = aventurier.getCoordoneeY();
                    if (this.coordonneX + 1 == x && this.coordonneY == y) {
                        aventurier.subirDegats(this.degat);
                    }
                    if (this.coordonneX - 1 == x && this.coordonneY == y) {
                        aventurier.subirDegats(this.degat);
                    }
                    if (this.coordonneX == x && this.coordonneY + 1 == y) {
                        aventurier.subirDegats(this.degat);
                    }
                    if (this.coordonneX == x && this.coordonneY - 1 == y) {
                        aventurier.subirDegats(this.degat);
                    }
                    this.nbBoucle = 0;
                    res = true;
                }else{
                    nbBoucle++;
                }
            return res;
        }else{
            return  false;
        }
    }

    /**
     * méthode qui permet au monstre de perdre de la vie
     * suite à une attaque
     * @param degats nombre de dégats subits
     */
    public void subirDegats(int degats) {
        int res = this.pv - degats;
        if(res <= 0){
            this.pv = 0;
            this.vivant = false;
            this.lab.retirerMort();
            Cases c = lab.getCase(this.coordonneX,this.coordonneY);
            c.setOccupee(false);
        }else {
            this.pv -= degats;
        }
        this.blesser = true;
    }

    /**
     * permet de récupérer l'attribut privé x
     * @return l'attribut x
     */
    public int getCoordonneX() {
        return coordonneX;
    }

    /**
     * permet de récupérer l'attribut privé y
     * @return l'attribut y
     */
    public int getCoordoneeY(){
        return coordonneY;
    }

    /**
     * permet de récupérer l'attribut privé lab
     * @return le labyrinthe dans lequel le monstre se trouve
     */
    public Labyrinthe getLab() {
        return lab;
    }

    /**
     * permet de récupérer l'attribut privé pv
     * @return les points de vie du monstre
     */
    public int getPv() {
        return pv;
    }

    /**
     * permet de savoir si le monstre est vivant ou non
     * @return le statut du monstre
     */
    public boolean isVivant() {
        return vivant;
    }

    /**
     * differenciation des sprites
     * @return boolean
     */
    @Override
    public boolean isStatue() {
        return true;
    }

    /**
     * differenciation des sprites
     * @return boolean
     */
    @Override
    public boolean isIntelligent() {
        return false;
    }

    /**
     * permet de changer l'attribut blesser selon l'etat du monstre
     * @param blesser
     */
    @Override
    public void setBlesser(boolean blesser) {
        this.blesser = blesser;
    }

    /**
     * boolean pour savoir si le monstre a subit des dégats
     * @return boolean blesser
     */
    @Override
    public boolean isBlesser() {
        return blesser;
    }
}
