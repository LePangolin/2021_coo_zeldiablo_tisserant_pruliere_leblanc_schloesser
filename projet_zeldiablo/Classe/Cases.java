package Classe;

import java.awt.*;

public interface Cases{

    /**
     * permet de récupérer l'attribut privé x
     * @return l'attribut x
     */
    public int getX();

    /**
     * permet de récupérer l'attribut privé y
     * @return l'attribut y
     */
    public int getY();

    /**
     * permet de récupérer l'attribut privé mur
     * @return l'attribut mur
     */
    public boolean etreMur();

    /**
     * permet de savoir si une entité se trouve sur la case
     * @return true si la case est occupée
     */
    public boolean isOccupee();

    /**
     * permet de changer le statut d'occupation d'une case
     * @param b true si occupée
     */
    public void setOccupee(boolean b);

    /**
     * permet de savoir si une case est piégée
     * @return true si piégée
     */
    public boolean isPiege();

    /**
     * permet de rendre une case piegée
     * @param p true pour la rendre piegée
     */
    public void setPiege(boolean p);

    /**
     * retourne l'attribut privé debut
     * @return le statut de la case
     */
    public boolean isDebut();

    /**
     * permet de définir si une case correpond au début
     * @param debut statut que l'on va donner à la case
     */
    public void setDebut(boolean debut);

    /**
     * permet de récupérer l'attribut privé escalier
     * @return le statut de la case
     */
    public boolean isEscalier();

    /**
     * permet de changer le statut d'une case pour en faire un escalier
     * @param e le nouveau statut de la case
     */
    public void setEscalier(boolean e);
}