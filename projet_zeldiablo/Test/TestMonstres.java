package Test;

import Classe.Aventurier;
import Classe.Labyrinthe;
import Classe.MonstreAleatoire;
import Classe.MonstreImmobile;
import org.junit.*;
import static org.junit.Assert.*;

public class TestMonstres {

Labyrinthe lab = new Labyrinthe(10,10);


    @Test
    public void testSubirDegats(){
        MonstreImmobile monstreImmobile = new MonstreImmobile(5,3, lab);
        monstreImmobile.subirDegats(5);
        assertEquals("La vie du monstre devrait etre égal a : ", 5, monstreImmobile.getPv());
    }

    @Test
    public void testSubirDegatMortel(){
        MonstreImmobile monstreImmobile = new MonstreImmobile(5,3, lab);
        monstreImmobile.subirDegats(10);
        assertFalse("Le monstre devrait être mort", monstreImmobile.isVivant());
    }


    @Test
    public void testAttaquerAventurier(){
        MonstreImmobile monstreImmobile = new MonstreImmobile(3,3, lab);
        lab.addList(monstreImmobile);
        Aventurier aventurier = new Aventurier(null, lab);
        for(int i =  0; i<11; i++) {
            monstreImmobile.attaquer(aventurier);
        }
        assertEquals("Les pv de "+aventurier.getNom()+" devrait avoir baisser",10, aventurier.getPv());
    }

    @Test
    public void testSeDeplacecAlea(){
        MonstreAleatoire monstreAleatoire = new MonstreAleatoire(5,5,lab);
        lab.addList(monstreAleatoire);
        for (int i = 0; i < 6; i++) {
            monstreAleatoire.seDeplacer();
        }
        boolean res = false;
        if (5 != monstreAleatoire.getCoordoneeY() || 5!= monstreAleatoire.getCoordonneX()){
            res = true;
        }
        assertTrue("La coordonnée X devrait etre differente de ", res);
    }









}
