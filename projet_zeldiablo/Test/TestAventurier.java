package Test;
import Classe.*;
import org.junit.*;
import static org.junit.Assert.*;

public class TestAventurier {

    @Test
    public void testCreationPersonnageAleatoire(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier("Jean Michel",labyrinthe);
        int pv = aventurier.getPv();
        int dgt = aventurier.getDegat();
        String nom = aventurier.getNom();
        boolean estVivant = aventurier.isVivant();
        int x = aventurier.getCoordonneX();
        int y = aventurier.getCoordoneeY();
        assertEquals("Les pvs devrait être égals a 10",10,pv);
        assertEquals("Les dégats devrait être égals a 4",4,dgt);
        assertTrue("L'aventurier devrait être viviant", estVivant);
        assertEquals("Le nom de l'aventurier devrait être \"Jean MIchel\"","Jean Michel", nom);
        assertEquals("La coordonnée X de base devrait être 1", 1, x);
        assertEquals("La coordonnée Y de base devrait être 1", 1, y);
    }

   @Test
    public  void testCreationPersonnageNomNull(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        String[] noms = aventurier.getNomSiNull();
        int i = 0;
        boolean nomdansTableau = false;
        while(i != 4 && !nomdansTableau) {
            if(noms[i].equals(aventurier.getNom())){
                nomdansTableau = true;
            }
            i++;
        }
        assertEquals("Le nom " + aventurier.getNom() + " n'est pas dans le tableau de génération aléatoire",true, nomdansTableau);
    }

    @Test
    public void testSeDeplacer(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        aventurier.seDeplacer(5,7);
        assertEquals(aventurier.getNom()+" devrait se trouve en 6 pour la coordonée X", 6, aventurier.getCoordonneX());
        assertEquals(aventurier.getNom()+" devrait se trouve en 8 pour la coordonée Y", 8, aventurier.getCoordoneeY());
    }

    @Test
    public void seDeplacerHorsLabyrintheNegatif(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        aventurier.seDeplacer(-2,-2);
        assertEquals(aventurier.getNom()+" ne devrait pas s'être deplacer", 1, aventurier.getCoordonneX());
        assertEquals(aventurier.getNom()+" ne devrait pas s'être deplacer", 1, aventurier.getCoordoneeY());

    }

    @Test
    public void seDeplacerEnDehorsLabyrinthePlus(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        aventurier.seDeplacer(11,11);
        assertEquals(aventurier.getNom()+" ne devrait pas s'être deplacer", 1, aventurier.getCoordonneX());
        assertEquals(aventurier.getNom()+" ne devrait pas s'être deplacer", 1, aventurier.getCoordoneeY());
    }


    @Test
    public void testSubirDegat(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        aventurier.subirDegats(6);
        assertEquals(aventurier.getNom()+" devrait avoir 4 pv", 4,aventurier.getPv());
    }

    @Test
    public void testSubirDegatMortel(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        aventurier.subirDegats(10);
        assertFalse(aventurier.getNom()+" devrait être mort", aventurier.isVivant());
    }

    @Test
    public void testAttaquer(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        MonstreImmobile monstreImmobile = new MonstreImmobile(1,2, labyrinthe);
        labyrinthe.addList(monstreImmobile);
        aventurier.attaquer();
        assertEquals("Les pv du monstre devrait avoir baissé ", 6, monstreImmobile.getPv());
    }

    @Test
    public void testAmulette(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        aventurier.recupererAmulette();
        assertEquals("L'amullette devrait être ramasser", labyrinthe.getAm().etrePrise(), aventurier.getAmulette().etrePrise());
    }

    @Test
    public void testHealAmulette(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Aventurier aventurier = new Aventurier(null, labyrinthe);
        aventurier.subirDegats(5);
        aventurier.recupererAmulette();
        assertEquals("Les pv de "+aventurier.getNom()+" devrait être a 10", 10, aventurier.getPv());
    }


}
