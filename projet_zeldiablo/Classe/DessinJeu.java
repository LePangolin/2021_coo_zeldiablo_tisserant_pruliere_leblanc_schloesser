package Classe;

import javax.imageio.*;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class DessinJeu extends JPanel implements moteurJeu.DessinJeu {

    /**
     * Parametre jeu de la classe
     */
    private Jeu jeu;
    /**
     * Sprite du mur
     */
    private BufferedImage mur;
    /**
     * Sprite du sol
     */
    private BufferedImage sol;
    /**
     * Sprite du piege
     */
    private BufferedImage piege;
    /**
     * Sprite du monstreImmobile
     */
    private BufferedImage statue;
    /**
     * Sprite de l'amulette
     */
    private BufferedImage collier;
    /**
     * Sprite de la case de départ
     */
    private BufferedImage debut;
    /**
     * Sprite de l'escalier
     */
    private BufferedImage escalier;
    /**
     * Sprite du personnage mort
     */
    private BufferedImage mort;
    /**
     * Sprite du monstreAleatoire
     */
    private BufferedImage monstre;
    /**
     * Sprite du monstreAttire
     */
    private BufferedImage monstreIntelligent;
    /**
     * int qui compte le nombre de frame ou le monstre doit être afficher comme blesser
     */
    private int frameBlesser = 0;
    /**
     * int qui compte le nombre le nombre de frame ou le hero doit être blesser
     */
    private int frameBlesserHero = 0;
    /**
     * int qui compte le nombre d'iteration permettant de différentier le spawn du déplacement
     */
    int it = 0;

    /**
     * Constructeur de DessinJeu prenant un objet Jeu en parametre
     * @param jeu :  Jeu qui doit être dessiner
     */
    public DessinJeu(Jeu jeu){
        this.jeu = jeu;
    }

    /**
     * Affiche les images en fonction du fichier text
     * @param image :  image du jeu a crée
     */
    public void spawn(BufferedImage image){
        Aventurier aventurier = jeu.getPj();
        Labyrinthe labyrinthe = jeu.getLabyrinthe();
        Graphics graphics = image.getGraphics();
        try {
            BufferedReader lecteur = new BufferedReader(new FileReader("projet_zeldiablo/Niveaux/" + this.jeu.getNomNiveau(this.jeu.getIndice())));
            this.mur = ImageIO.read(new File("projet_zeldiablo/images/Arbre2.png"));
            this.sol = ImageIO.read(new File("projet_zeldiablo/images/Herbe.png"));
            this.piege = ImageIO.read(new File("projet_zeldiablo/images/PiegeAOurs.png"));
            BufferedImage personnage = ImageIO.read(new File("projet_zeldiablo/images/Personnage.png"));
            this.statue = ImageIO.read(new File("projet_zeldiablo/images/Statue.png"));
            this.collier = ImageIO.read(new File("projet_zeldiablo/images/collier.png"));
            this.debut = ImageIO.read((new File("projet_zeldiablo/images/Depart.png")));
            this.escalier = ImageIO.read(new File("projet_zeldiablo/images/EscalierDescendre.jpeg"));
            this.mort = ImageIO.read(new File("projet_zeldiablo/images/PersonnageMort.png"));
            this.monstre = ImageIO.read(new File("projet_zeldiablo/images/Monstre.png"));
            this.monstreIntelligent = ImageIO.read(new File("projet_zeldiablo/images/MonstreIntelligent.png"));
            int tailleX = Integer.parseInt(lecteur.readLine());
            int tailleY = Integer.parseInt(lecteur.readLine());
            int j = 0;
            int nbligne= 0;
            String ligne = null;
            while((ligne = lecteur.readLine()) != null){
                int nbcharactere = 0;    //x
                for (int i=0;i<ligne.length(); i++){
                    char ch= ligne.charAt(i);
                    if(labyrinthe.getCase(nbcharactere,nbligne).isDebut()){
                        graphics.drawImage(this.debut,labyrinthe.getAv().getCoordonneX()*40, labyrinthe.getAv().getCoordoneeY() * 40, 40, 40,null);
                    }
                    if (ch=='X') {
                        graphics.drawImage(this.sol,nbcharactere*40,nbligne*40,40,40, null);
                        graphics.drawImage(mur,nbcharactere*40,nbligne*40,40,40,null);
                    }else{
                        graphics.drawImage(this.sol,nbcharactere*40,nbligne*40,40,40, null);
                    }
                    if(ch=='M'){
                        graphics.drawImage(this.statue,labyrinthe.getEntite(j).getCoordonneX() * 40, labyrinthe.getEntite(j).getCoordoneeY() * 40, 30, 30, null);
                        j++;
                    }
                    if(ch=='P'){
                        graphics.drawImage(this.sol,nbcharactere*40,nbligne*40,40,40, null);
                        graphics.drawImage(this.piege,nbcharactere*40, nbligne * 40, 40, 40,null);
                    }
                    if(ch=='A'){
                        graphics.drawImage(personnage,labyrinthe.getAv().getCoordonneX() * 41, labyrinthe.getAv().getCoordoneeY() * 40, 25, 40,null);
                    }
                    if(ch=='R'){
                        graphics.drawImage(this.monstre,labyrinthe.getEntite(j).getCoordonneX() * 40, labyrinthe.getEntite(j).getCoordoneeY() * 40, 30, 30, null);
                        j++;
                    }
                    if(ch=='U'){
                        graphics.drawImage(this.collier,labyrinthe.getAm().getX() * 40, labyrinthe.getAm().getY() * 40, 40, 40, null);
                    }
                    if(ch=='E'){
                        graphics.drawImage(this.escalier,nbcharactere * 40, nbligne * 40, 40, 40, null);
                    }
                    if(ch=='I'){
                        graphics.drawImage(this.monstreIntelligent,labyrinthe.getEntite(j).getCoordonneX() * 40, labyrinthe.getEntite(j).getCoordoneeY() * 40, 30, 30, null);
                        j++;
                    }
                    nbcharactere ++;
                }
                nbligne ++;
            }
            lecteur.close();

        }catch (IOException e){
            e.printStackTrace();
        }
    }
    /**
     * Affiche les images en fonction du déplacement et des touches appuyer
     * @param image :  image du jeu a crée
     */
    public void bouger(BufferedImage image) throws IOException{
        Graphics graphics = image.getGraphics();
        Labyrinthe lab = jeu.getLabyrinthe();
        for (int i = 0; i < lab.getTailleX(); i++) {
            for (int j = 0; j < lab.getTailleY(); j++) {
                graphics.drawImage(this.sol, i * 40, j * 40, 40, 40, null);
                if (lab.getCase(i, j).isDebut()) {
                    graphics.drawImage(this.debut, i * 40, j * 40, 40, 40, null);
                }
                if (lab.getCase(i, j).isPiege()) {
                    graphics.drawImage(this.piege, i * 40, j * 40, 40, 40, null);
                }
                if (lab.getCase(i, j).etreMur()) {
                    graphics.drawImage(mur, i * 40, j * 40, 40, 40, null);
                }
                if (lab.getCase(i, j).isEscalier()) {
                    graphics.drawImage(this.escalier, i * 40, j * 40, 40, 40, null);
                }
                for (int k = 0; k < lab.getTailleListe(); k++) {
                    Monstre monstre = lab.getEntite(k);
                    String res = null;
                    if(!monstre.isStatue()){
                        res = "aleatoire";
                    }else if((monstre.isIntelligent())){
                        res = "nemesis";
                    }else{
                        res = "statue";
                    }
                    switch (res) {
                        case "aleatoire" -> {
                            graphics.drawImage(this.monstre, monstre.getCoordonneX() * 40, monstre.getCoordoneeY() * 40, 30, 30, null);
                            if(monstre.isBlesser()){
                                frameBlesser++;
                                graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/MonstreBlesser.png")), monstre.getCoordonneX() * 40, monstre.getCoordoneeY() * 40, 30, 30, null);
                                if (frameBlesser == 240) {
                                    monstre.setBlesser(false);
                                    frameBlesser = 0;
                                }
                            }
                        }
                        case "nemesis" -> {
                            graphics.drawImage(this.monstreIntelligent, monstre.getCoordonneX() * 40, monstre.getCoordoneeY() * 40, 30, 30, null);
                            if(monstre.isBlesser()){
                                frameBlesser++;
                                graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/MonstreIntelligentBlesser.png")), monstre.getCoordonneX() * 40, monstre.getCoordoneeY() * 40, 30, 30, null);
                                if (frameBlesser == 240) {
                                    monstre.setBlesser(false);
                                    frameBlesser = 0;
                                }
                            }
                        }
                        case "statue"->{
                            graphics.drawImage(this.statue, monstre.getCoordonneX() * 40, monstre.getCoordoneeY() * 40, 30, 30, null);
                            if(monstre.isBlesser()){
                                frameBlesser++;
                                graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/StatueBlesser.png")), monstre.getCoordonneX() * 40, monstre.getCoordoneeY() * 40, 30, 30, null);
                                if (frameBlesser == 240) {
                                    monstre.setBlesser(false);
                                    frameBlesser = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
        Aventurier aventurier = jeu.getPj();
        String direction = aventurier.getDirection();
        switch (direction) {
            case "bas" -> {
                if (!aventurier.isBlesser()) {
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/Personnage.png")), aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
                }else{
                    frameBlesserHero++;
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/PersonnageBlesserBas.png")), aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
                    if(frameBlesserHero == 1) {
                        aventurier.setBlesser(false);
                        frameBlesserHero = 0;
                    }
                }
            }
            case "gauche" -> {
                if (!aventurier.isBlesser()) {
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/PersonnageGauche.png")), aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
                }else{
                    frameBlesserHero++;
                    System.out.println(frameBlesserHero);
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/PersonnageGaucheBlesser.png")), aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
                    if(frameBlesserHero == 1) {
                        aventurier.setBlesser(false);
                        frameBlesserHero = 0;
                    }
                }
            }
            case "droite" -> {
                if (!aventurier.isBlesser()) {
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/Personnagedroite.png")), aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
                }else{
                    frameBlesserHero++;
                    System.out.println(frameBlesserHero);
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/PersonnagedroiteBlesser.png")), aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
                    if(frameBlesserHero == 1) {
                        aventurier.setBlesser(false);
                        frameBlesserHero = 0;
                    }
                }
            }
            case "haut" -> {
                if (!aventurier.isBlesser()) {
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/PersonnageDos.png")), aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
                }else{
                    frameBlesserHero++;
                    System.out.println(frameBlesserHero);
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/PersonnageDosBlesser.png")), aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
                    if(frameBlesserHero == 1) {
                        aventurier.setBlesser(false);
                        frameBlesserHero = 0;
                    }
                }
            }

        }
        if (!aventurier.isVivant()) {
            graphics.drawImage(this.mort, aventurier.getCoordonneX() * 41, aventurier.getCoordoneeY() * 40, 25, 40, null);
            graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/lose.png")), 220, 20, 360, 360, null);
        }
        if (lab.getAm() != null) {
            Amulette amulette = lab.getAm();
            if (!amulette.etrePrise()) {
                graphics.drawImage(this.collier, amulette.getX() * 40, amulette.getY() * 40, 40, 40, null);
            }
        }
        if (jeu.isGagner()) {
            graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/win.png")), 220, 20, 360, 360, null);
        }
        int pv = aventurier.getPv();
        int actualCoeur = 0;
        int nbCoeur = 0;
        if (pv > 0) {
            while (actualCoeur <= pv) {
                graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/CoeurRemplie.png")), 800 - (nbCoeur * 40), 0, 40, 40, null);
                actualCoeur += 2;
                nbCoeur++;
            }

            if (actualCoeur > pv && nbCoeur <= 5) {
                if (actualCoeur % pv == 1) {
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/CoeurBlesser.png")), 800 - (nbCoeur * 40), 0, 40, 40, null);
                } else if(pv == 1){
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/CoeurBlesser.png")), 800 - (nbCoeur * 40), 0, 40, 40, null);
                }else{
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/CoeurVide.png")), 800 - (nbCoeur * 40), 0, 40, 40, null);
                }
                nbCoeur++;
            }
            if (nbCoeur <= 5) {
                while (nbCoeur <= 5) {
                    graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/CoeurVide.png")), 800 - (nbCoeur * 40), 0, 40, 40, null);
                    nbCoeur++;
                }
            }
        }else{
            while(nbCoeur != 6){
                graphics.drawImage(ImageIO.read(new File("projet_zeldiablo/images/CoeurVide.png")), 800 - (nbCoeur * 40), 0, 40, 40, null);
                nbCoeur++;
            }
        }
    }

    /**
     * Methode qui gère l'affichage du jeu a tout les stades
     * @param image :  image du jeu a dessiner
     */
    @Override
    public void dessiner(BufferedImage image) {
       Graphics g = image.getGraphics();
       if(this.it == 0){
           this.spawn(image);
           it++;
       }
       try {
           this.bouger(image);
       }catch (IOException e){
           e.printStackTrace();
       }
    }
}
