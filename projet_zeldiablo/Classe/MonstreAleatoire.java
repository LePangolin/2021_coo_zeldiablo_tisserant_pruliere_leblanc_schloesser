package Classe;

import moteurJeu.Commande;

import java.util.Random;

public class MonstreAleatoire implements Monstre{


    /**
     * points de vie du monstre
     */
    private int pv;
    /**
     * coordonnées du monstre dans le labyrinthe
     */
    private int coordonneX,coordonneY;
    /**
     * puissance d'attaque du monstre
     */
    private int degat;
    /**
     * portée d'attaque du monstre
     */
    private final int portee =  1;
    /**
     * labyrinthe où se trouve le monstre
     */
    private Labyrinthe lab;
    /**
     * indique si le monstre est en vie ou non
     */
    private boolean vivant;
    /**
     * int permettant de récupérer le nombre de boucles réalisées
     */
    private int nbBoucle = 0;

    /**
     * int permettant de récupérer le nombre de boucles réalisées
     */
    private int nbBoucle1 = 0;
    /**
     *Boolean qui test si le monstre est blesser
     */
    private boolean blesser;

    /**
     * constructeur d'un monstre
     * @param x position x
     * @param y position y
     * @param laby labyrinthe où se trouve le monstre
     */
    public MonstreAleatoire(int x, int y, Labyrinthe laby){
        this.coordonneX = x;
        this.coordonneY = y;
        this.degat =2;
        this.pv = 10;
        this.vivant = true;
        this.lab = laby;
        this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
    }

    /**
     * permet au monstre de se déplacer aléatoirement dans le labyrinthe
     */
    @Override
    public void seDeplacer(){
        if(vivant) {
            if (this.nbBoucle1 == 5) {
                Random random = new Random();
                int pos = 1 + random.nextInt(5 - 1) ;

                switch (pos) {
                    //+1 en x
                    case 1:
                        //on récupere les cordonnée de la nouvelle case
                        int nouvelleCaseX = this.coordonneX + 1;
                        int nouvelleCaseY = this.coordonneY + 0;
                        //Si la case n'est pas un mur l'aventurier se déplace et va sur cette case sinon rien ne se passe
                        if ((nouvelleCaseX >= 0 && nouvelleCaseY >= 0 && (nouvelleCaseX <= lab.getTailleX()-1 && nouvelleCaseY <= lab.getTailleY()-1)) && !lab.getCase(nouvelleCaseX, nouvelleCaseY).isOccupee()) {
                            if ((!lab.getCase(nouvelleCaseX, nouvelleCaseY).etreMur())) {
                                this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(false);
                                this.coordonneX += 1;
                                this.coordonneY += 0;
                                this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
                            }
                        }
                        break;
                    //-1 en x
                    case 2:
                        //on récupere les cordonnée de la nouvelle case
                        nouvelleCaseX = this.coordonneX - 1;
                        nouvelleCaseY = this.coordonneY + 0;
                        //Si la case n'est pas un mur l'aventurier se déplace et va sur cette case sinon rien ne se passe
                        if ((nouvelleCaseX >= 0 && nouvelleCaseY >= 0 && (nouvelleCaseX <= lab.getTailleX()-1 && nouvelleCaseY <= lab.getTailleY()-1)) && !lab.getCase(nouvelleCaseX, nouvelleCaseY).isOccupee()) {
                            if ((!lab.getCase(nouvelleCaseX, nouvelleCaseY).etreMur())) {
                                this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(false);
                                this.coordonneX -= 1;
                                this.coordonneY += 0;
                                this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
                            }
                        }
                        break;
                    //+1 en y
                    case 3:
                        //on récupere les cordonnée de la nouvelle case
                        nouvelleCaseX = this.coordonneX + 0;
                        nouvelleCaseY = this.coordonneY + 1;
                        //Si la case n'est pas un mur l'aventurier se déplace et va sur cette case sinon rien ne se passe
                        if ((nouvelleCaseX >= 0 && nouvelleCaseY >= 0 && (nouvelleCaseX <= lab.getTailleX()-1 && nouvelleCaseY <= lab.getTailleY()-1)) && !lab.getCase(nouvelleCaseX, nouvelleCaseY).isOccupee()) {
                            if ((!lab.getCase(nouvelleCaseX, nouvelleCaseY).etreMur())) {
                                this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(false);
                                this.coordonneX += 0;
                                this.coordonneY += 1;
                                this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
                            }
                        }
                        break;
                    //-1 en y
                    case 4:
                        //on récupere les cordonnée de la nouvelle case
                        nouvelleCaseX = this.coordonneX + 0;
                        nouvelleCaseY = this.coordonneY - 1;
                        //Si la case n'est pas un mur l'aventurier se déplace et va sur cette case sinon rien ne se passe
                        if ((nouvelleCaseX >= 0 && nouvelleCaseY >= 0 && (nouvelleCaseX <= lab.getTailleX()-1 && nouvelleCaseY <= lab.getTailleY()-1)) && !lab.getCase(nouvelleCaseX, nouvelleCaseY).isOccupee()) {
                            if ((!lab.getCase(nouvelleCaseX, nouvelleCaseY).etreMur())) {
                                this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(false);
                                this.coordonneX += 0;
                                this.coordonneY -= 1;
                                this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
                            }
                        }
                        break;
                }
                this.nbBoucle1 = 0;
            } else {
                this.nbBoucle1++;
            }

        }
    }

    /**
     * Methode qui permet au monstre de se deplacer vers l'aventurier
     * @param c : commande qui dès que presser, permet au monstre de se deplacer
     */
    @Override
    public void seDeplacerVersAcenturier(Commande c) {

    }

    /**
     * permet au monstre d'attaquer
     * @param aventurier l'aventurier qui subira l'attaque
     * @return true si l'attaque a été portée
     */
    @Override
    public boolean attaquer(Aventurier aventurier){
        boolean res = false;
            if (vivant) {
                if (aventurier.getLab() != this.lab) {
                    res = false;
                } else if (this.nbBoucle == 10) {
                    int x = aventurier.getCoordonneX();
                    int y = aventurier.getCoordoneeY();
                    this.nbBoucle = nbBoucle + 1;
                    if (this.coordonneX + 1 == x && this.coordonneY == y) {
                        aventurier.subirDegats(this.degat);
                    }
                    if (this.coordonneX - 1 == x && this.coordonneY == y) {
                        aventurier.subirDegats(this.degat);
                    }
                    if (this.coordonneX == x && this.coordonneY + 1 == y) {
                        aventurier.subirDegats(this.degat);
                    }
                    if (this.coordonneX == x && this.coordonneY - 1 == y) {
                        aventurier.subirDegats(this.degat);
                    }
                    this.nbBoucle = 0;
                    res = true;
                } else {
                    nbBoucle++;
                }
            }
        return res;
    }

    /**
     * permet d'enlever de la vie selon les dégats subit
     * @param degats nombre de dégats subits
     */
    public void subirDegats(int degats) {
        int res = this.pv - degats;
        if(res <= 0){
            this.pv = 0;
            this.vivant = false;
            this.lab.retirerMort();
            Cases c = lab.getCase(this.coordonneX,this.coordonneY);
            c.setOccupee(false);
        }else {
            this.pv -= degats;
        }
        blesser = true;

    }

    /**
     * permet de récupérer l'attribut privé x
     * @return l'attribut x
     */
    public int getCoordonneX() {
        return coordonneX;
    }

    /**
     * permet de récupérer l'attribut privé y
     * @return l'attribut y
     */
    public int getCoordoneeY(){
        return coordonneY;
    }

    /**
     * permet de récupérer l'attribut privé lab
     * @return le labyrinthe dans lequel le monstre se trouve
     */
    public Labyrinthe getLab() {
        return lab;
    }

    /**
     * permet de récupérer l'attribut privé pv
     * @return les points de vie du monstre
     */
    public int getPv() {
        return pv;
    }

    /**
     * permet de savoir si le monstre est vivant ou non
     * @return le statut du monstre
     */
    public boolean isVivant() {
        return vivant;
    }

    /**
     * differenciation des sprites
     * @return boolean
     */
    @Override
    public boolean isStatue() {
        return false;
    }

    /**
     * differenciation des sprites
     * @return boolean
     */
    @Override
    public boolean isIntelligent() {
        return false;
    }

    /**
     * permet de changer l'attribut blesser selon l'etat du monstre
     * @param blesser
     */
    @Override
    public void setBlesser(boolean blesser) {
        this.blesser = blesser;
    }

    /**
     * boolean pour savoir si le monstre a subit des dégats
     * @return boolean blesser
     */
    @Override
    public boolean isBlesser() {
        return blesser;
    }
}
