package Classe;


import moteurJeu.Commande;

import java.util.Random;

public class MonstreAttire implements Monstre{


    /**
     * points de vie du monstre
     */
    private int pv;
    /**
     * coordonnées du monstre dans le labyrinthe
     */
    private int coordonneX,coordonneY;
    /**
     * puissance d'attaque du monstre
     */
    private int degat;
    /**
     * portée d'attaque du monstre
     */
    private final int portee =  1;
    /**
     * labyrinthe où se trouve le monstre
     */
    private Labyrinthe lab;
    /**
     * indique si le monstre est en vie ou non
     */
    private boolean vivant;
    /**
     * int permettant de récupérer le nombre de boucles réalisées
     */
    private int nbBoucle = 0;

    /**
     * int permettant de récupérer le nombre de boucles réalisées
     */
    private int nbBoucle1 = 0;

    /**
     * boolean pour savoir si le monstre a subit des dégats
     */
    private boolean blesser;

    /**
     * constructeur d'un monstre
     * @param x position x
     * @param y position y
     * @param laby labyrinthe où se trouve le monstre
     */
    public MonstreAttire(int x, int y, Labyrinthe laby){
        this.coordonneX = x;
        this.coordonneY = y;
        this.degat =3;
        this.pv = 10;
        this.vivant = true;
        this.lab = laby;
        this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
    }

    /**
     * permet au monstre de se déplacer aléatoirement dans le labyrinthe
     */
    @Override
    public void seDeplacer(){
        //vide car une autre méthode est utilisée
    }

    /**
     * permet de se déplacer vers l'aventurier
     * à chaque fois que ce dernier bouge
     */
    @Override
    public void seDeplacerVersAcenturier(Commande c) {
        int avX;
        int avY;
        int manathanDistanceAv;

        if (c.haut || c.bas || c.droite || c.gauche){

            manathanDistanceAv = Math.abs(lab.getAv().getCoordonneX() - this.coordonneX) + Math.abs(lab.getAv().getCoordoneeY() - this.coordonneY);
            if (manathanDistanceAv > 1) {
                if (Math.abs(lab.getAv().getCoordonneX() - this.coordonneX + 1) + Math.abs(lab.getAv().getCoordoneeY() - this.coordonneY) > manathanDistanceAv && !lab.getCase(this.coordonneX + 1, this.coordonneY).etreMur() && !lab.getCase(this.coordonneX + 1, this.coordonneY).isOccupee()) {
                    this.lab.getCase(this.coordonneX, this.coordonneY).setOccupee(false);
                    this.coordonneX += 1;
                    this.lab.getCase(this.coordonneX, this.coordonneY).setOccupee(true);

                } else if (Math.abs(lab.getAv().getCoordonneX() - this.coordonneX - 1) + Math.abs(lab.getAv().getCoordoneeY() - this.coordonneY) > manathanDistanceAv && !lab.getCase(this.coordonneX - 1, this.coordonneY).etreMur() && !lab.getCase(this.coordonneX - 1, this.coordonneY).isOccupee()) {
                    this.lab.getCase(this.coordonneX, this.coordonneY).setOccupee(false);
                    this.coordonneX -= 1;
                    this.lab.getCase(this.coordonneX, this.coordonneY).setOccupee(true);

                } else if (Math.abs(lab.getAv().getCoordonneX() - this.coordonneX) + Math.abs(lab.getAv().getCoordoneeY() - this.coordonneY + 1) > manathanDistanceAv && !lab.getCase(this.coordonneX, this.coordonneY + 1).etreMur() && !lab.getCase(this.coordonneX, this.coordonneY + 1).isOccupee()) {
                    this.lab.getCase(this.coordonneX, this.coordonneY).setOccupee(false);
                    this.coordonneY += 1;
                    this.lab.getCase(this.coordonneX, this.coordonneY).setOccupee(true);

                } else if (Math.abs(lab.getAv().getCoordonneX() - this.coordonneX) + Math.abs(lab.getAv().getCoordoneeY() - this.coordonneY - 1) > manathanDistanceAv && !lab.getCase(this.coordonneX, this.coordonneY - 1).etreMur() && !lab.getCase(this.coordonneX + 1, this.coordonneY - 1).isOccupee()) {
                    this.lab.getCase(this.coordonneX, this.coordonneY).setOccupee(false);
                    this.coordonneY -= 1;
                    this.lab.getCase(this.coordonneX, this.coordonneY).setOccupee(true);
                }
            }
        }
    }

    /**
     * permet au monstre d'attaquer
     * @param aventurier l'aventurier qui subira l'attaque
     * @return true si l'attaque a été portée
     */
    @Override
    public boolean attaquer(Aventurier aventurier){
        boolean res = false;
        if (vivant) {
            if (aventurier.getLab() != this.lab) {
                res = false;
            } else if (this.nbBoucle == 8) {
                int x = aventurier.getCoordonneX();
                int y = aventurier.getCoordoneeY();
                this.nbBoucle = nbBoucle + 1;
                if (this.coordonneX + 1 == x && this.coordonneY == y) {
                    aventurier.subirDegats(this.degat);
                }
                if (this.coordonneX - 1 == x && this.coordonneY == y) {
                    aventurier.subirDegats(this.degat);
                }
                if (this.coordonneX == x && this.coordonneY + 1 == y) {
                    aventurier.subirDegats(this.degat);
                }
                if (this.coordonneX == x && this.coordonneY - 1 == y) {
                    aventurier.subirDegats(this.degat);
                }
                this.nbBoucle = 0;
                res = true;
            } else {
                nbBoucle++;
            }
        }
        return res;
    }

    /**
     * permet d'enlever de la vie selon les dégats subit
     * @param degats nombre de dégats subits
     */
    public void subirDegats(int degats) {
        int res = this.pv - degats;
        if(res <= 0){
            this.pv = 0;
            this.vivant = false;
            this.lab.retirerMort();
            Cases c = lab.getCase(this.coordonneX,this.coordonneY);
            c.setOccupee(false);
        }else {
            this.pv -= degats;
        }
        blesser = true;

    }

    /**
     * permet de récupérer l'attribut privé x
     * @return l'attribut x
     */
    public int getCoordonneX() {
        return coordonneX;
    }

    /**
     * permet de récupérer l'attribut privé y
     * @return l'attribut y
     */
    public int getCoordoneeY(){
        return coordonneY;
    }

    /**
     * permet de récupérer l'attribut privé lab
     * @return le labyrinthe dans lequel le monstre se trouve
     */
    public Labyrinthe getLab() {
        return lab;
    }

    /**
     * permet de récupérer l'attribut privé pv
     * @return les points de vie du monstre
     */
    public int getPv() {
        return pv;
    }

    /**
     * permet de savoir si le monstre est vivant ou non
     * @return le statut du monstre
     */
    public boolean isVivant() {
        return vivant;
    }

    /**
     * differenciation des sprites
     * @return boolean
     */
    @Override
    public boolean isStatue() {
        return true;
    }

    /**
     * differenciation des sprites
     * @return boolean
     */
    @Override
    public boolean isIntelligent() {
        return true;
    }

    /**
     * permet de changer l'attribut blesser selon l'etat du monstre
     * @param blesser
     */
    @Override
    public void setBlesser(boolean blesser) {
        this.blesser = blesser;
    }

    /**
     * differenciation des sprites
     * @return boolean
     */
    public boolean isItelligent() {
        return true;
    }

    /**
     * boolean pour savoir si le monstre a subit des dégats
     * @return boolean blesser
     */
    @Override
    public boolean isBlesser() {
        return blesser;
    }
}

