package Test;
import Classe.*;
import moteurJeu.Commande;
import org.junit.*;
import static org.junit.Assert.*;

public class TestCases {

    @Test
    public void testCasePleine(){
        CasePleine c = new CasePleine();
        boolean m = c.etreMur();
        assertEquals("La case doit être un mur",true,m);
    }
    @Test
    public void testCaseLibre(){
        CaseLibre c = new CaseLibre();
        boolean m = c.etreMur();
        assertEquals("La case doit être libre",false,m);
    }

    @Test
    public void testCasePleineCoo(){
        CasePleine c = new CasePleine(3,4);
        boolean m = c.etreMur();
        int x = c.getX();
        int y = c.getY();
        assertEquals("La position x doit etre 3",3,x);
        assertEquals("La position y doit etre 4",4,y);
    }
    @Test
    public void testCaseLibreCoo(){
        CaseLibre c = new CaseLibre(4,5);
        boolean m = c.etreMur();
        int x = c.getX();
        int y = c.getY();
        assertEquals("La position x doit etre 4",4,x);
        assertEquals("La position y doit etre 5",5,y);
    }

    @Test
    public void testCasePleine_CooNeg(){
        CasePleine c = new CasePleine(-3,-4);
        boolean m = c.etreMur();
        int x = c.getX();
        int y = c.getY();
        assertEquals("Les coordonnées sont invalides",0,x);
        assertEquals("Les coordonnées sont invalides",0,y);
    }
    @Test
    public void testCaseLibre_CooNrg(){
        CaseLibre c = new CaseLibre(-4,-5);
        boolean m = c.etreMur();
        int x = c.getX();
        int y = c.getY();
        assertEquals("Les coordonnées sont invalides",0,x);
        assertEquals("Les coordonnées sont invalides",0,y);
    }
    @Test
    public void testCaseOccupee(){
        Labyrinthe l = new Labyrinthe(10,10);
        Aventurier av = new Aventurier("Guts",l);
        Cases c = l.getCase(1,1);
        assertTrue("La Case devrait etre occuper ",c.isOccupee());
    }
    @Test
    public void testCasePlusOccupee(){
        Labyrinthe l = new Labyrinthe(10,10);
        Aventurier av = new Aventurier("Guts",l);
        Commande c = new Commande();
        c.bas = true;
        av.deplacer(c);
        Cases ce = l.getCase(1,1);

        assertFalse("La Case ne devrait pas etre occuper ",ce.isOccupee());
    }
}
