package Classe;

import java.awt.*;

public class CaseLibre implements Cases {
    /**
     * position de la case sur l'axe horizontal
     */
    private int x;
    /**
     * position de la case sur l'axe vertical
     */
    private int y;
    /**
     * indique si la case est un mur ou non
     */
    private boolean mur;
    /**
     * Indique si la case est occupé
     */
    private boolean occupee;

    /**
     * indique la case est piegée
     */
    private boolean piege;
    /**
     * indique si la case correspond au début du niveau
     */
    private boolean debut;
    /**
     * indique si la case est un escalier
     */
    private boolean escalier;

    /**
     * constructeur d'une case libre
     */
    public CaseLibre(){
        mur = false;
        occupee = false;
        debut = false;
    }

    /**
     * constructeur d'une case libre à des coordonnées données
     * @param absc position x
     * @param ord position y
     */
    public CaseLibre(int absc,int ord){
        if(absc > 0){
            x = absc;
        }else{
            x = 0;
        }
        if(ord > 0){
            y = ord;
        }else{
            y = 0;
        }
        mur = false;
        occupee = false;
    }

    /**
     * permet de récupérer l'attribut privé x
     * @return l'attribut x
     */
    public int getX(){
        return this.x;
    }

    /**
     * permet de récupérer l'attribut privé y
     * @return l'attribut y
     */
    public int getY(){
        return this.y;
    }

    /**
     * permet de récupérer l'attribut privé mur
     * @return l'attribut mur
     */
    public boolean etreMur(){
        return this.mur;
    }

    /**
     * permet de savoir si une entité se trouve sur la case
     * @return true si la case est occupée
     */
    public boolean isOccupee(){
        return this.occupee;
    }

    /**
     * permet de changer le statut d'occupation d'une case
     * @param b true si occupée
     */
    public void setOccupee(boolean b){
        this.occupee = b;
    }

    /**
     * permet de savoir si une case est piégée
     * @return true si piégée
     */
    @Override
    public boolean isPiege() {
        return this.piege;
    }

    /**
     * permet de rendre une case piegée
     * @param p true pour la rendre piegée
     */
    @Override
    public void setPiege(boolean p) {
        this.piege = p;
    }

    /**
     * retourne l'attribut privé debut
     * @return le statut de la case
     */
    public boolean isDebut() {
        return debut;
    }

    /**
     * permet de définir si une case correpond au début
     * @param debut statut que l'on va donner à la case
     */
    public void setDebut(boolean debut) {
        this.debut = debut;
    }

    /**
     * permet de récupérer l'attribut privé escalier
     * @return le statut de la case
     */
    public boolean isEscalier(){
        return escalier;
    }

    /**
     * permet de changer le statut d'une case pour en faire un escalier
     * @param e le nouveau statut de la case
     */
    public void setEscalier(boolean e){
        this.escalier = e;
    }
}