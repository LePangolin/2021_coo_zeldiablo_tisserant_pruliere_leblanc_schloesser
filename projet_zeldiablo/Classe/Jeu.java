package Classe;
import moteurJeu.Commande;


public class Jeu implements moteurJeu.Jeu {

    /**
     * Labyrinthe dans lequel le jeu se déroule
     */
    private Labyrinthe labJeu;
    /**
     * Aventurier qui joue la partie
     */
    private Aventurier aventurier;
    /**
     * tableau des noms des différents niveaux
     */
    private String[] nomNiveau = {"niveau1.txt","niveau2.txt","niveau3.txt"};
    /**
     * tableau des différents niveaux
     */
    private Labyrinthe[] niveaux;
    /**
     * indice permettant de connaitre le niveau dans lequel l'aventurier se trouve
     */
    private int indice = 0;
    /**
     * boolean qui permet de voir si le jeu a été gagner
     */
    private boolean gagner;

    /**
     * Constructeur vide d'un jeu
     */
    public Jeu() {
        this.aventurier = new Aventurier(null);
        niveaux = new Labyrinthe[nomNiveau.length];
        for(int i=0;i <nomNiveau.length;i++){
            niveaux[i] = new Labyrinthe(nomNiveau[i], this.aventurier);
        }
        this.gagner = false;
        this.labJeu = niveaux[indice];
        this.aventurier.setLab(labJeu);
    }

    /**
     * Actualise le jeu pour que celui ci puisse progresser
     *
     * @param commandeUser
     */
    @Override
    public void evoluer(Commande commandeUser) {
        this.getPj().deplacer(commandeUser);
        if (commandeUser.attaquer) {
            this.getPj().attaquer();
        }
        for (int i = 0; i < labJeu.getTailleListe(); i++) {
            Monstre m = labJeu.getEntite(i);
            m.attaquer(this.getPj());
            m.seDeplacer();
            m.seDeplacerVersAcenturier(commandeUser);
        }
        if (commandeUser.prendre) {
            if(labJeu.getAm() != null){
                this.getPj().recupererAmulette();
            }
            this.monter();
            //this.descendre();
        }
        if (commandeUser.descendre) {
            this.descendre();
        }
        if (aventurier.getAmulette() != null && labJeu.getCase(aventurier.getCoordonneX(), aventurier.getCoordoneeY()).isDebut()) {
            gagner = true;
        }
    }

    /**
     * indique si le jeu est fini ou non
     *
     * @return
     */
    @Override
    public boolean etreFini() {
        boolean res = false;
        if (!aventurier.isVivant()) {
            res = true;
        }
        if (this.gagner) {
            res = true;
        }
        return res;
    }

    /**
     * permet de récupérer l'attribut privé aventurier
     *
     * @return l'aventurier qui est en jeu
     */
    public Aventurier getPj() {
        return this.aventurier;
    }

    /**
     * permet de récupérer l'attribut labJeu
     *
     * @return le labyrinthe dans lequel le jeu en cours se déroule
     */
    public Labyrinthe getLabyrinthe() {
        return this.labJeu;
    }

    /**
     * permet de récuprérer l'attribut privé nomNiveau à une place donnée
     * dans le tableau
     * @param i l'indice du tableau
     * @return le nom du niveau à l'indice
     */
    public String getNomNiveau(int i) {
        return nomNiveau[i];
    }

    /**
     * permet de récupérer l'attribut privé indice
     * @return l'indice du niveau dans lequel l'aventurier se trouve
     */
    public int getIndice() {
        return indice;
    }

    /**
     * permet à l'aventurier de prendre l'escalier pour monter
     */
    public void monter() {
        int x = aventurier.getCoordonneX();
        int y = aventurier.getCoordoneeY();
        if (indice < nomNiveau.length - 1) {
            if (labJeu.getCase(x, y).isEscalier() && niveaux[indice+1].getCase(x, y).isEscalier()) {
                    indice += 1;
                    this.labJeu = niveaux[indice];
                    this.aventurier.setLab(labJeu);
            }
        }
    }

    /**
     * permet à l'aventurier de prendre l'escalier pour descendre
     */
    public void descendre() {
        int x = aventurier.getCoordonneX();
        int y = aventurier.getCoordoneeY();
        if (indice > 0) {
            if (labJeu.getCase(x, y).isEscalier() && niveaux[indice-1].getCase(x, y).isEscalier()) {
                    indice -= 1;
                    this.labJeu = niveaux[indice];
                    this.aventurier.setLab(labJeu);
            }
        }
    }

    /**
     *Getteur qui vérifie si le jeu est gagné
     * @return true si le jeu est gagné
     */
    public boolean isGagner() {
        return gagner;
    }
}
