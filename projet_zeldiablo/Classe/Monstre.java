package Classe;

import moteurJeu.Commande;

public interface Monstre {


    /**
     * permet à l'entité de se déplacer aléatoirement
     */
    public void seDeplacer();

    /**
     * permet de se déplacer vers l'aventurier
     * à chaque fois que ce dernier bouge
     */
    public void seDeplacerVersAcenturier(Commande c);


    /**
     * permet à l'entité d'attaquer
     * @return true si l'attaque a pu être portée
     */
    public boolean attaquer(Aventurier aventurier);

    /**
     * fait perdre de la vie à l'entité
     * @param degats nombre de dégats subits
     */
    public void subirDegats(int degats);

    /**
     * permet de récupérer l'attribut privé x
     * @return l'attribut x
     */
    public int getCoordonneX();

    /**
     * permet de récupérer l'attribut privé y
     * @return l'attribut y
     */
    public int getCoordoneeY();

    /**
     * permet de récupérer l'attribut privé lab
     * @return l'attribut lab
     */
    public Labyrinthe getLab();

    /**
     * permet de récupérer l'attribut privé pv
     * @return les points de vie du monstre
     */
    public int getPv();

    /**
     * permet de savoir si le monstre est vivant ou non
     * @return le statut du monstre
     */
    public boolean isVivant();

    /**
     * differenciation des sprites
     * @return boolean
     */
    public boolean isStatue();

    /**
     * differenciation des sprites
     * @return boolean
     */
    public boolean isIntelligent();

    /**
     * differenciation des sprites
     * @return boolean
     */
    public void setBlesser(boolean blesser);

    /**
     * differenciation des sprites
     * @return boolean
     */
    public boolean isBlesser();
}
