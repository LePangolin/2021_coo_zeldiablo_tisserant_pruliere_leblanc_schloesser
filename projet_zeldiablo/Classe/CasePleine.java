package Classe;

import java.awt.*;

public class CasePleine implements Cases{
    /**
     * position de la case sur l'axe horizontal
     */
    private int x;
    /**
     * position de la case sur l'axe vertical
     */
    private int y;
    /**
     * indique si la case est un mur ou non
     */
    private boolean mur;

    /**
     * constructeur d'une case pleine
     */
    public CasePleine(){
        mur = true;
    }

    /**
     * constructeur d'une case pleine à des coordonnées données
     * @param absc position x
     * @param ord position y
     */
    public CasePleine(int absc,int ord){
        if(absc > 0){
            x = absc;
        }else{
            x = 0;
        }
        if(ord > 0){
            y = ord;
        }else{
            y = 0;
        }
        mur = true;
    }

    /**
     * permet de récupérer l'attribut privé x
     * @return l'attribut x
     */
    public int getX(){
        return this.x;
    }

    /**
     * permet de récupérer l'attribut privé y
     * @return l'attribut y
     */
    public int getY(){
        return this.y;
    }

    /**
     * permet de récupérer l'attribut privé mur
     * @return l'attribut mur
     */
    public boolean etreMur(){
        return this.mur;
    }

    /**
     * permet de savoir si une entité se trouve sur la case
     * @return true si la case est occupée
     */
    @Override
    public boolean isOccupee() {
        return false;
    }

    /**
     * vide car la case est déjà occupée
     * @param b true si occupée
     */
    @Override
    public void setOccupee(boolean b){ }

    /**
     * permet de savoir si une case est piégée
     * @return true si piégée
     */
    public boolean isPiege(){
        return false;
    }

    /**
     * permet de rendre une case piegée
     * @param b true pour la rendre piegée
     */
    public void setPiege(boolean b){}

    /**
     * retourne le statut de la case
     * @return false car une case pleine ne
     * peut être le début du niveau
     */
    @Override
    public boolean isDebut() {
        return false;
    }

    @Override
    public void setDebut(boolean c) {
        //vide car une case pleine ne peut être un début
    }

    /**
     * permet de savoir si la case est un escalier
     * @return false car une case pleine ne peut pas être un escalier
     */
    public boolean isEscalier(){
        return false;
    }

    /**
     * Methode permettant de mettre un case en tant qu'escalier
     * @param e le nouveau statut de la case
     */
    public void setEscalier(boolean e){
        //vide car une case pleine ne peut être un escalier
    }
}