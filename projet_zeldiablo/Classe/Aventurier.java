package Classe;

import moteurJeu.Commande;

import java.util.Random;

public class Aventurier{
    /**
     * nom donné a l'aventurier
     */
    private String nom;
    /**
     * points de vie du joueur
     */
    private int pv;
    /**
     * indique si le joueur est en vie ou non
     */
    private boolean vivant;
    private Random rand;
    /**
     * noms qui seront donné au joueur si le nom rentré est null
     */
    private final String[] nomSiNull = {"ZeroTwo","Eugeo","Gilbert","Guts"};
    /**
     * coordonnées du joueur dans le labyrinthe
     */
    private int coordonneX, coordonneY;
    /**
     * labyrinthe où se trouve le joueur
     */
    private Labyrinthe lab;
    /**
     * portée d'attaque du joueur
     */
    private final int portee =  1;
    /**
     * puissance d'attaque du joueur
     */
    private int degat;

    /**
     * String qui stock la dernière direction de l'aventurier
     */
    private String direction;

    /**
     * Attribut amulette de l'aventurier, necessaire pour la conditions de victoire
     */
    private Amulette amulette;

    /**
     * booléen qui regarde si l'aventurier a subis des dégats
     */
    private boolean blesser;

    /**
     * Créer un nouvel aventurier
     * @param nom le nom du joueur créer
     * @param laby labyrinthe où se trouvera le joueur
     */
    public Aventurier(String nom, Labyrinthe laby){
        rand = new Random();
        if(nom == null){
            int indice = rand.nextInt(4);
            this.nom = nomSiNull[indice];
        }else{
            this.nom = nom;
        }
        //this.inventaire = new Inventaire();
        this.degat =4;
        this.pv = 10;
        this.vivant = true;
        this.lab = laby;
        this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
        this.direction = "bas";
        this.amulette = null;
        this.coordonneX = 1;
        this.coordonneY = 1;
    }

    /**
     * Constructeur d'aventurier prenant en parmettre son nom
     * @param nom : nom de l'aventurier
     */
    public Aventurier(String nom){
        rand = new Random();
        if(nom == null){
            int indice = rand.nextInt(4);
            this.nom = nomSiNull[indice];
        }else{
            this.nom = nom;
        }
        //this.inventaire = new Inventaire();
        this.degat =4;
        this.pv = 10;
        this.vivant = true;
        this.lab = new Labyrinthe(10,10);
        this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);
        this.direction = "bas";
        this.amulette = null;
        this.coordonneX = 1;
        this.coordonneY = 1;
    }
    /**
     * permet à l'aventurier de se déplacer dans le labyrinthe
     * @param x direction horizontale
     * @param y direction verticale
     */
    public void seDeplacer(int x, int y){
        if(vivant) {
            //on récupere les cordonnée de la nouvelle classe
            int nouvelleCaseX = this.coordonneX + x;
            int nouvelleCaseY = this.coordonneY + y;
            //Si la case n'est pas un mur l'aventurier se déplace et va sur cette case sinon rien ne se passe
            if ((nouvelleCaseX >= 0 && nouvelleCaseY >= 0 && (nouvelleCaseX <= lab.getTailleX()-1 && nouvelleCaseY <= lab.getTailleY()-1)) && !lab.getCase(nouvelleCaseX, nouvelleCaseY).isOccupee()) {
                if ((!lab.getCase(nouvelleCaseX, nouvelleCaseY).etreMur())) {
                    this.coordonneX += x;
                    this.coordonneY += y;
                    if (lab.getCase(coordonneX, coordonneY).isPiege()) {
                        //Pensez a desactiver après test
                        this.subirDegats(3);
                    }
                }
            }
        }
    }

    /**
     * permet à l'aventurier d'attaquer
     * @return true si l'attaque a pu être portée
     */
    public boolean attaquer(){
        boolean solution = false;
            int caseAAttaquer;
            String res = this.direction;
            switch (res) {
                case "bas" -> {
                    caseAAttaquer = coordonneY + portee;
                    Monstre monstre = lab.getMonstre(this.coordonneX, caseAAttaquer);
                    if (monstre != null) {
                        monstre.subirDegats(this.degat);
                        solution = true;
                    }
                }
                case "haut" -> {
                    caseAAttaquer = coordonneY - portee;
                    Monstre monstre = lab.getMonstre(this.coordonneX, caseAAttaquer);
                    if (monstre != null) {
                        monstre.subirDegats(this.degat);
                        solution = true;
                    }
                }
                case "droite" -> {
                    caseAAttaquer = coordonneX + portee;
                    Monstre monstre = lab.getMonstre(caseAAttaquer, this.coordonneY);
                    if (monstre != null) {
                        monstre.subirDegats(this.degat);
                        solution = true;
                    }
                }
                case "gauche" -> {
                    caseAAttaquer = coordonneX - portee;
                    Monstre monstre = lab.getMonstre(caseAAttaquer, this.coordonneY);
                    if (monstre != null) {
                        monstre.subirDegats(this.degat);
                        solution = true;
                    }
                }
            }
        return solution;
    }

    /**
     * fait perdre de la vie à l'aventurier
     * @param degat nombre de dégats subits par le joueur
     */
    public void subirDegats(int degat){
        int res = this.pv - degat;
        if(res <= 0){
            this.pv = 0;
            this.vivant = false;
        }else {
            this.pv -= degat;
        }
        if(this.pv == 0){
            this.vivant = false;
        }
        this.blesser = true;
    }

    /**
     * permet de récupérer l'attribut privé x
     * @return l'attribut x
     */
    public int getCoordonneX() {
        return coordonneX;
    }

    public void setCoordonneX(int x){
        this.coordonneX =x;
    }
    /**
     * permet de récupérer l'attribut privé y
     * @return l'attribut y
     */
    public int getCoordoneeY(){
        return coordonneY;
    }

    public void setCoordonneY(int y ){
        this.coordonneY = y;
    }

    /**
     * permet de récupérer l'attribut privé lab
     * @return l'attribut lab
     */
    public Labyrinthe getLab() {
        return lab;
    }

    public void setLab(Labyrinthe l){
        this.lab = l;
    }
    /**
     * permet de récupérer l'attribut privé pv
     * @return l'attribut pv
     */
    public int getPv() {
        return pv;
    }


    /**
     * permet de récupérer l'attribut privé nom
     * @return l'attribut nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * permet de récupérer l'attribut privé degat
     * @return l'attribut degat
     */
    public int getDegat() {
        return degat;
    }

    /**
     * permet de récupérer l'attribut privé vivant
     * @return l'attribut vivant
     */
    public boolean isVivant() {
        return vivant;
    }

    /**
     * permet de récupérer l'attribut privé nomSiNull
     * @return l'attribut nomSiNull
     */
    public String[] getNomSiNull() {
        return nomSiNull;
    }

    /**
     * permet a l'avanturier de se déplacer dans le labyrinthe
     * @param c touche utilisée pour le déplacement
     */
    public void deplacer(Commande c){
        if(c.bas){
            this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(false);
            this.seDeplacer(0,1);
            this.direction = "bas";
        }else if(c.haut){
            this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(false);
            this.seDeplacer(0,-1);
            this.direction = "haut";
        }else if(c.droite){
            this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(false);
            this.seDeplacer(1,0);
            this.direction = "droite";
        }else if(c.gauche){
            this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(false);
            this.seDeplacer(-1,0);
            this.direction  = "gauche";
        }
        this.lab.getCase(this.coordonneX,this.coordonneY).setOccupee(true);

    }

    /**
     * Methode qui permet de récuperer l'amulette si l'aventurier se trouve sur la même case que l'amulette
     * @return true si l'amulette a été prise
     */
    public boolean recupererAmulette(){
        Amulette am = lab.getAm();
        int x = am.getX();
        int y = am.getY();
        if(this.coordonneX == x && this.coordonneY == y){
            this.amulette = am;
            am.setPrise(true);
            this.pv = 10;
            return  true;
        }else{
            return  false;
        }
    }

    /**
     * getteur qui permet de retouner l'amullette de l'aventurier
     * @return amulette associé a l'aventurier, null si il n'en possede pas
     */
    public Amulette getAmulette() {
        return amulette;
    }

    /**
     * getteur qui retourne la direction dans laquelle regarde l'aventurier
     * @return String de la direction de l'aventurier
     */
    public String getDirection() {
        return direction;
    }

    /**
     * Permet de changer l'attribut blesser du monstre afin de gérer le sprite du personnage
     * @param blesser
     */
    public void setBlesser(boolean blesser) {
        this.blesser = blesser;
    }

    /**
     * getteur qui vérifie si le monstre a été blesser
     * @return
     */
    public boolean isBlesser() {
        return blesser;
    }

}
