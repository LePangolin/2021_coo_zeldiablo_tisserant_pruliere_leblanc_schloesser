package Classe;

public class Amulette {
    /**
     * positions de l'amulette dans le labyrinthe
     */
    private int x,y;
    /**
     * permet de savoir si elle est prise par l'aventurier
     */
    private boolean prise = false;

    /**
     * créer une nouvelle amulette
     * @param abs position x
     * @param ord position y
     */
    public Amulette(int abs, int ord){
        this.x = abs;
        this.y = ord;
    }

    /**
     * permet de savoir si l'amulette est prise
     * @return le statut de l'amulette
     */
    public boolean etrePrise(){
        return prise;
    }

    /**
     * change la valeur de prise
     * @param b le nouveau statut de l'amulette
     */
    public void setPrise(boolean b){
        this.prise = b;
    }

    /**
     * permet de récupérer l'attribut privé x
     * @return x
     */
    public int getX(){
        return this.x;
    }

    /**
     * permet de récupérer l'attribut privé y
     * @return y
     */
    public int getY() {
        return y;
    }
}
