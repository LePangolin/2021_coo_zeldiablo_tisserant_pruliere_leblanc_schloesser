package Classe;

import moteurJeu.MoteurGraphique;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Jeu jeu = new Jeu();
        DessinJeu dessinJeu = new DessinJeu(jeu);
        MoteurGraphique moteurGraphique = new MoteurGraphique(jeu, dessinJeu);
        moteurGraphique.lancerJeu(800,400);
        System.out.println("Le jeu est fini");
    }
}
