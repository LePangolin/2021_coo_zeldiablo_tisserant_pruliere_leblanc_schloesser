Fonctionnalité 1 : Initialiser le Labyrinthe - Lilian LEBLANC, Justine PRULIERE
    Difficulter : *
    Au lancement du jeu, un labyrinthe par défaut est créé et le personnage est placé
    au centre de celui-ci.
        • Le labyrinthe par défaut est constitué d'une arène de taille 10*10 ;
        • Le personnage est placé au centre de l'arène ;
        • Les monstres (s'il y en a) sont placés sur des cases vides (à adapter).

Fonctionnalité 2 : Collision avec les murs - Paul TISSERANT, Lilian LEBLANC
    Lorsque le joueur demande à déplacer le personnage en direction d'un mur,
    le personnage reste bloqué et ne se déplace pas.
    Critères de validation
        • La collision fonctionne dans les 4 directions ;
        • Si la case est vide, le personnage avance.

Fonctionnalité 3 : Initialisation du hero - Adrien SCHLOESSER
    Definir des caractérique et les methode de base du hero
	- Nom
        - point de vie de base
        - attaque de base
        - se déplacer


Fonctionnalité 4 : Initialisation d'un Monstre - Paul TISSERANT
    Definir des caractérique et les methode de base d'un monstre
        - point de vie de base
        - attaque de base
        - se déplacer