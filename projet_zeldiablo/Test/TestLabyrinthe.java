package Test;
import Classe.*;
import org.junit.*;
import static org.junit.Assert.*;

public class TestLabyrinthe {
    @Test
    public void testCreationLabyrinthe(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        for(int i=0;i < 10 ; i++){
            for(int j=0;j < 10 ; j++){
                Cases c = labyrinthe.getCase(i,j);
                assertEquals("Les Cases n'ont pas les bonnes coordonée en abscisse ",i,c.getX());
                assertEquals("Les Cases n'ont pas les bonnes coordonée en ordonnée ",j,c.getY());
            }
        }
    }
    @Test
    public void testCaseEnDehorsLaby(){
        Labyrinthe labyrinthe = new Labyrinthe(10,10);
        Cases c =labyrinthe.getCase(12,12);
        Cases c1 =labyrinthe.getCase(-12,12);
        Cases c2 =labyrinthe.getCase(12,-12);
        Cases c3 =labyrinthe.getCase(-12,-12);
        assertTrue("La Case devrait etre pleine ",c.etreMur());
        assertTrue("La Case devrait etre pleine ",c1.etreMur());
        assertTrue("La Case devrait etre pleine ",c2.etreMur());
        assertTrue("La Case devrait etre pleine ",c3.etreMur());

    }
}
